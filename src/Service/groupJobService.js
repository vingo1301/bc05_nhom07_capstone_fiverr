import axios from "axios"
import { BASE_URL, config } from "./configURL"

export const groupJobService = {
    getLoaiCongViec: () => {
        return axios({
            url: BASE_URL + "/api/loai-cong-viec",
            method: "GET",
            headers: config
            }
        )
    },
    getMenuCongViec: () => {
        return axios({
            url: BASE_URL + "/api/cong-viec/lay-menu-loai-cong-viec",
            method: "GET",
            headers: config
        })
    },
    xoaCongViecDaThue: (id,token) => {
        return axios({
            url: BASE_URL + `/api/thue-cong-viec/${id}`,
            method: "DELETE",
            headers: {...config,token:token}
        })
    }
}