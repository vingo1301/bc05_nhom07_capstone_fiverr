import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import Comments from "../Components/Comments/Comments";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";
import Introduction from "../Components/Introduction/Introduction";
import Market from "../Components/Market/Market";
import Popular from "../Components/Popular/Popular";
import { Desktop, Mobile, Tablet } from "../HOC/Responsive/Responsive";
import { setGroupJob } from "../Redux-toolkit/slice/jobSlice";
import { groupJobService } from "../Service/groupJobService";

export default function HomePage() {
  let dispatch = useDispatch();
  useEffect(() => {
    groupJobService
      .getLoaiCongViec()
      .then((res) => {
        dispatch(setGroupJob(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const removeActive = () => {
    document.getElementById("navBar")?.classList.remove("active");
    document.getElementById("mobileNavBar")?.classList.remove("active");
  };
  const scrolled = () => {
    let Yoffset = window.pageYOffset;
    if (Yoffset == 0) {
      document.getElementById("navBar")?.classList.remove("active");
      document.getElementById("mobileNavBar")?.classList.remove("active");
    } else {
      document.getElementById("navBar")?.classList.add("active");
      document.getElementById("mobileNavBar")?.classList.add("active");
    }
  };
  return (
    <>
      <div onLoad={removeActive}>
        <Header></Header>
        {(window.onscroll = scrolled)}
        <Popular></Popular>
        <Introduction></Introduction>
        <Comments></Comments>
        <Market></Market>
        <Footer></Footer>
      </div>
    </>
  );
}
