export const validEmty = (value) => {
    let result = {
        valid: null,
        message: ""
    }
    if (value.length == 0){
        return result = {
            valid: false,
            message: "Cannot be emty!"
        }
    }else{
        return result = {
            valid: true,
            message: ""
        }
    }
}
export const validLetterAndSpace = (value) => {
    let result = {
        valid: null,
        message: ""
    }
    const regex =  /^[a-zA-Z\s]*$/;
    let isValid = regex.test(value);
    if (isValid){
        return result = {
            valid: true,
            message: ""
        }
    }
    else{
        return result = {
            valid: false,
            message: "Please Input Letters and Spacing"
        }
    }
}
export const validEmail = (value) => {
    let result = {
        valid: null,
        message: ""
    }
    const regex =  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isValid = regex.test(value);
    if (isValid){
        return result = {
            valid: true,
            message: ""
        }
    }
    else{
        return result = {
            valid: false,
            message: "Please Input abc@xyz.com"
        }
    }
}
export const validPassword = (value) => {
    let result = {
        valid: null,
        message: ""
    }
    const regex =  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
    let isValid = regex.test(value);
    if (isValid){
        return result = {
            valid: true,
            message: ""
        }
    }
    else{
        return result = {
            valid: false,
            message: "Atleast 8 characters, 1 digit, 1 Uppercase, 1 lowercase"
        }
    }
}
export const validNumber = (value) => {
    let result = {
        valid: null,
        message: ""
    }
    const regex =  /^\d+$/;
    let isValid = regex.test(value);
    if (isValid){
        return result = {
            valid: true,
            message: ""
        }
    }
    else{
        return result = {
            valid: false,
            message: "Only Number"
        }
    }
}
export const validDate = (value) => {
    let result = {
        valid: null,
        message: ""
    }
    const regex =  /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/;
    let isValid = regex.test(value);
    if (isValid){
        return result = {
            valid: true,
            message: ""
        }
    }
    else{
        return result = {
            valid: false,
            message: "yyyy-mm-dd"
        }
    }
}