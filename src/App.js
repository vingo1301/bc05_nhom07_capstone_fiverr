import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import Layout from "./HOC/Layout/Layout";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import Profile from "./Pages/Profile/Profile";
import WorkList from "./Components/WorkList/WorkList";
import GroupWorkList from "./Components/GroupWorkList/GroupWorkList";
import JobDetail from "./Components/JobDetail/JobDetail";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage></HomePage>}></Route>
          <Route
            path="/login"
            element={<Layout Component={LoginPage}></Layout>}
          ></Route>
          <Route
            path="/register"
            element={<Layout Component={RegisterPage}></Layout>}
          ></Route>
          <Route
            path="/profile"
            element={<Layout Component={Profile}></Layout>}
          ></Route>
          <Route
            path="/categories/:id"
            element={<Layout Component={WorkList}></Layout>}
          ></Route>
          <Route
            path="/title/:groupId"
            element={<Layout Component={GroupWorkList}></Layout>}
          ></Route>
          <Route
            path="/jobDetail/:id"
            element={<Layout Component={JobDetail}></Layout>}
          ></Route>
          <Route
            path="/result/:jobName"
            element={<Layout Component={WorkList}></Layout>}
          ></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
