import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import "../Header.css";
import {userLocalService} from '../../../Service/localService'
import { groupJobService } from '../../../Service/groupJobService';
import { setMenuJob } from '../../../Redux-toolkit/slice/jobSlice';

export default function MobileNavBar() {
  let dispatch = useDispatch();
    let user = useSelector((state) => state.userSlice.user);
    let menuJob = useSelector((state) => state.jobSlice.menuJob)
    const [searchInput, setSearchInput] = useState();
    let handleChangeInput = (event) => {
    setSearchInput(event.target.value);
    console.log(searchInput);
    };
    useEffect(() => {
      groupJobService.getMenuCongViec().then((res) => {
        console.log(res.data.content);
        dispatch(setMenuJob(res.data.content.slice(0,8)))
      })
      .catch((err) => {
       console.log(err);
      });
    },[])
    const handleShowMenu = () => {
        document.getElementById("subMenu").classList.remove("w-0");
        document.getElementById("subMenu").classList.add("w-full");
    }
    const handleHideMenu = () => {
        document.getElementById("subMenu").classList.remove("w-full");
        document.getElementById("subMenu").classList.add("w-0");
    }
    const handleLogout = () => {
       userLocalService.remove();
       window.location.reload();
    }
    const hanldeShowMenuItem = (id) => {
        document.getElementById(`${id}`).classList.toggle("hidden");
        document.getElementById(`${id}Show`).classList.toggle("hidden");
        document.getElementById(`${id}Hidden`).classList.toggle("hidden");
    }
    const renderUser = () => {
        if(user){
            return <div className='grid grid-cols-3 text-center '>
                <div className='col-span-2'>
                    <a href="/profile">
                    <p className='text-2xl pl-2 text-green-600 font-semibold'><i class="fa fa-user mr-3"></i>{user.user.name}</p></a>
                </div>
                <div>
                    <button onClick={handleLogout} className='py-2 px-3 bg-red-500 text-white font-semibold rounded-lg'><a href="/">Log Out</a></button>
                </div>
            </div>
        }else{
            return(
                <div className='text-center'>
                    <button className='py-2 px-3 bg-green-500 text-white font-semibold mr-3 rounded-lg'><a href="/login">Sign In</a></button>
                    <button className='py-2 px-3 bg-red-500 text-white font-semibold rounded-lg'><a href="/register">Join Us</a></button>
                </div>
            )
        }
    }
    let renderSubmenu = () => {
        return(
            menuJob.map((loaiCongViec) => {
                return  <li className='relative p-3' onClick={() => {hanldeShowMenuItem(loaiCongViec.id)}}>
                    <div className='flex justify-between'>
                        <a className='relative font-semibold text-xl' href={`/title/${loaiCongViec.id}`}>{loaiCongViec.tenLoaiCongViec}</a>
                        <div>
                            <i id={`${loaiCongViec.id}Show`} className="fa fa-angle-down text-right"></i>
                            <i id={`${loaiCongViec.id}Hidden`} className="hidden fa fa-angle-up"></i>
                        </div>
                    </div>
                    <ul id={`${loaiCongViec.id}`} className='hidden'>
                    {loaiCongViec.dsNhomChiTietLoai.map((nhomViec) => {
                        return(
                            <li>
                                <div className='p-2'>
                                    <p>{nhomViec.tenNhom}</p>
                                    {nhomViec.dsChiTietLoai.map((item) => {
                                    return(
                                        <div className='my-2'>
                                            <a className='text-md text-gray-600 relative' href={`/categories/${item.id}`}>{item.tenChiTiet}</a>
                                        </div>
                                        ) 
                                    })}
                                </div>
                            </li>
                            )
                        })}
                    </ul>
                  </li>
              })
        )
    }
    const renderMenu = () => {
        return(
            <div id='subMenu' className='w-0 subMenu bg-white absolute top-0 left-0 mr-10 overflow-hidden'>
                <div className='w-full relative'>
                    <button onClick={handleHideMenu} className='text-3xl absolute top-2 right-5'><i class="fa fa-times"></i></button>
                </div>
                <div className='user py-3 mt-12 w-full bg-gray-200'>
                    {renderUser()}
                </div>
                <div className='subMenuMobile'>
                    <ul>
                        {renderSubmenu()}
                    </ul>
                </div>
            </div>
        )
    }
    const renderNavBar = () => {
        return(
            <div className='logo flex pl-5 py-3'>
                        <button onClick={handleShowMenu} className='text-white text-2xl mr-4'><i class="fa fa-bars"></i></button>
                        <a href="/">
                            <svg className='light' width="89" height="27" viewBox="0 0 89 27" fill="none" xmlns="http://www.w3.org/2000/svg"><g fill="#fff"><path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path></g><g fill="#1dbf73"><path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path></g></svg>
                            <svg className='dark' width="89" height="27" viewBox="0 0 89 27" fill="none" xmlns="http://www.w3.org/2000/svg"><g fill="#404145"><path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z"></path></g><g fill="#1dbf73"><path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z"></path></g></svg>
                        </a>
                    </div>
        )
    }
    const renderSearchBar = () => {
        return(
            <div className='header_search mx-10 mb-3'>
                <form className='flex justify-center' action="">
                    <input onChange={handleChangeInput} className='py-2 pl-3 rounded-l' type="text" placeholder='Find Services' />
                    {/* <button className='py-2 px-3 rounded-r text-white bg-green-500'>Search</button> */}
                    <a
                href={`/result/${searchInput}`}
                className="text-base py-3 bg-green-700 px-5 text-white font-bold rounded-r h-full"
              >
                Search
              </a>
                </form>
            </div>
        )
    }
  return (
    <div id='mobileNavBar' className='bg-transparent fixed w-full top-0 left-0 z-10 active'>
        <div className='navBar'>
            {renderNavBar()}
            {renderMenu()}
            {renderSearchBar()}
        </div>
    </div>
  )
}
